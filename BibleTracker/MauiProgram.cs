﻿using BibleTracker.Pages;
using BibleTracker.Pages.Trackers;
using BibleTracker.Services;
using BibleTracker.ViewModels;

namespace BibleTracker;

public static class MauiProgram
{
	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
				fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
			});

		builder.Services.AddSingleton<AboutPage>();
        builder.Services.AddSingleton<AboutViewModel>();

        builder.Services.AddSingleton<TrackerIndexPage>();
        builder.Services.AddSingleton<TrackerViewModel>();

		builder.Services.AddSingleton<AddTrackerPage>();
        builder.Services.AddSingleton<AddTrackerViewModel>();

		builder.Services.AddSingleton<TrackerDetailsPage>();
		builder.Services.AddSingleton<TrackerDetailsViewModel>();

        builder.Services.AddSingleton<EditTrackerPage>();
        builder.Services.AddSingleton<EditTrackerViewModel>();

        builder.Services.AddSingleton<TrackerService>();

        return builder.Build();
	}
}
