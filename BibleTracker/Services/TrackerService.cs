﻿using BibleTracker.Models;
using SQLite;
using System.Text.Json;

namespace BibleTracker.Services;

public class TrackerService
{
    List<Tracker> _trackers = new();
    List<Canon> _canons = new();
    List<Testament> _testaments = new();
    private List<Division> _divisions = new();
    private SQLiteConnection _dbConnection;

    public TrackerService()
    {
        var dbName = "Tracker.db3";
        var dbPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), dbName);
        _dbConnection = new SQLiteConnection(dbPath);

        //create/update tables
        _dbConnection.CreateTable<Division>();
        _dbConnection.CreateTable<Book>();
        _dbConnection.CreateTable<Canon>();
        _dbConnection.CreateTable<Testament>();   
        _dbConnection.CreateTable<Tracker>();
        _dbConnection.CreateTable<TrackerChapter>();

        //load data
        _canons = GetCanons();
        _testaments = GetTestaments();
        _divisions = GetDivisions();
    }

    public async Task<List<Tracker>> GetTrackers()
    {
        if (_trackers?.Count > 0)
        {
            return _trackers;
        }

        //testing purposes
        _trackers.Clear();
        using var stream = await FileSystem.OpenAppPackageFileAsync("trackerdata.json");
        using var reader = new StreamReader(stream);
        var contents = await reader.ReadToEndAsync();
        _trackers = JsonSerializer.Deserialize<List<Tracker>>(contents);

        return _trackers;
    }

    public void AddTracker(Tracker tracker)
    {
        if(tracker.Canon == null)
        {
            tracker.Canon = GetCanon(tracker.CanonId);
        }

        if(tracker.CanonId != tracker.Canon.Id)
        {
            tracker.CanonId = tracker.Canon.Id;
        }

        //Add the books/chapters for the selected canon
        _trackers.Add(tracker);
    }

    public void SaveTracker(Tracker tracker)
    {
        var index = tracker.Id;
        _trackers[index].Name = tracker.Name;
        _trackers[index].IsActive = tracker.IsActive;
    }

    public List<Division> GetDivisions()
    {
        if (_divisions?.Count > 0)
            return _divisions;

        var result = _dbConnection.Table<Division>().ToList();
        if (result.Any())
        {
            _divisions.Clear();
            foreach (var canon in result)
                _divisions.Add(canon);
        }
        else
            AddDefaultCanonData();

        return _divisions;
    }

    public void AddDefaultDivisions()
    {
        _divisions.Add(new Division() { Id = 1, CanonId = 1, TestamentId = 1, Name = "Pentateuch" });
        _divisions.Add(new Division() { Id = 2, CanonId = 1, TestamentId = 1, Name = "History" });
        _divisions.Add(new Division() { Id = 3, CanonId = 1, TestamentId = 1, Name = "Writings" });
        _divisions.Add(new Division() { Id = 4, CanonId = 1, TestamentId = 1, Name = "Major Prophets" });
        _divisions.Add(new Division() { Id = 5, CanonId = 1, TestamentId = 1, Name = "Minor Prophets" });
        _divisions.Add(new Division() { Id = 6, CanonId = 1, TestamentId = 2, Name = "Gospels" });
        _divisions.Add(new Division() { Id = 7, CanonId = 1, TestamentId = 2, Name = "History" });
        _divisions.Add(new Division() { Id = 8, CanonId = 1, TestamentId = 2, Name = "Pauline Epistles" });
        _divisions.Add(new Division() { Id = 9, CanonId = 1, TestamentId = 2, Name = "General Epistles" });
        _divisions.Add(new Division() { Id = 10, CanonId = 1, TestamentId = 2, Name = "Apocalypse" });

        _divisions.Add(new Division() { Id = 1, CanonId = 2, TestamentId = 1, Name = "Pentateuch" });
        _divisions.Add(new Division() { Id = 2, CanonId = 2, TestamentId = 1, Name = "Historical Books" });
        _divisions.Add(new Division() { Id = 3, CanonId = 2, TestamentId = 1, Name = "Prophetical Books" });
        _divisions.Add(new Division() { Id = 4, CanonId = 2, TestamentId = 1, Name = "Wisdom Literature" });

        _divisions.Add(new Division() { Id = 6, CanonId = 2, TestamentId = 2, Name = "Gospels" });
        _divisions.Add(new Division() { Id = 7, CanonId = 2, TestamentId = 2, Name = "History" });
        _divisions.Add(new Division() { Id = 8, CanonId = 2, TestamentId = 2, Name = "Pauline Epistles" });
        _divisions.Add(new Division() { Id = 9, CanonId = 2, TestamentId = 2, Name = "General Epistles" });
        _divisions.Add(new Division() { Id = 10, CanonId = 2, TestamentId = 2, Name = "Apocalypse" });


        _divisions.Add(new Division() { Id = 6, CanonId = 3, TestamentId = 2, Name = "Gospels" });
        _divisions.Add(new Division() { Id = 7, CanonId = 3, TestamentId = 2, Name = "History" });
        _divisions.Add(new Division() { Id = 8, CanonId = 3, TestamentId = 2, Name = "Pauline Epistles" });
        _divisions.Add(new Division() { Id = 9, CanonId = 3, TestamentId = 2, Name = "General Epistles" });
        _divisions.Add(new Division() { Id = 10, CanonId = 3, TestamentId = 2, Name = "Apocalypse" });
        _dbConnection.InsertAll(_divisions);
    }

    public Canon GetCanon(int id)
    {
        return _canons.Find(x => x.Id == id);
    }

    public List<Canon> GetCanons()
    {
        if(_canons?.Count > 0)
            return _canons;

        var result = _dbConnection.Table<Canon>().ToList();
        if (result.Any())
        {
            _canons.Clear();
            foreach (var canon in result)
                _canons.Add(canon);
        }
        else
            AddDefaultCanonData();

        return _canons;
    }

    public void AddDefaultCanonData()
    {
        _canons.Add(new Canon() { Id = 1, Name = "Protestant" });
        _canons.Add(new Canon() { Id = 2, Name = "Catholic" });
        _canons.Add(new Canon() { Id = 3, Name = "Orthodox" });
        _dbConnection.InsertAll(_canons);
    }

    public List<Testament> GetTestaments()
    {
        if (_testaments?.Count > 0)
            return _testaments;

        var result = _dbConnection.Table<Testament>().ToList();
        if (result.Any())
        {
            _testaments.Clear();
            foreach (var testament in result)
                _testaments.Add(testament);
        }
        else
            AddDefaultTestamentData();

        return _testaments;
    }

    public void AddDefaultTestamentData()
    {
        _testaments.Add(new Testament() { Id = 1, Name = "Old Testament" });
        _testaments.Add(new Testament() { Id = 1, Name = "New Testament" });
        _dbConnection.InsertAll(_testaments);
    }
}
