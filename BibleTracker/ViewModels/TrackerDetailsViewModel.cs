﻿using BibleTracker.Models;
using BibleTracker.Pages.Trackers;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace BibleTracker.ViewModels;

[QueryProperty(nameof(Tracker), "Tracker")]
public partial class TrackerDetailsViewModel : ObservableObject
{
    [ObservableProperty]
    public Tracker tracker;

    public TrackerDetailsViewModel()
    {

    }

    [RelayCommand]
    public async Task EditTracker()
    {
        await Shell.Current.GoToAsync(nameof(EditTrackerPage), true, new Dictionary<string, object>
        {
            {"Tracker", tracker }
        });
    }
}
