﻿using BibleTracker.Models;
using BibleTracker.Pages.Trackers;
using BibleTracker.Services;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace BibleTracker.ViewModels;

[QueryProperty(nameof(Tracker), "Tracker")]
public partial class EditTrackerViewModel : ObservableObject
{
    private TrackerService _trackerService;
    [ObservableProperty]
    public Tracker tracker;

    public EditTrackerViewModel(TrackerService trackerService)
    {
        _trackerService = trackerService;
    }

    [RelayCommand]
    public async void SaveTracker()
    {
        _trackerService.SaveTracker(tracker);
        await Shell.Current.GoToAsync("..");
    }

    [RelayCommand]
    public async void CancelTracker()
    {
        await Shell.Current.GoToAsync("..");
    }
}
