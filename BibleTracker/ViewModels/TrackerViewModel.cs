﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using BibleTracker.Models;
using BibleTracker.Services;
using System.Collections.ObjectModel;
using BibleTracker.Pages.Trackers;

namespace BibleTracker.ViewModels;

public partial class TrackerViewModel : ObservableObject
{
    public ObservableCollection<Tracker> Trackers { get; } = new();
    TrackerService _trackerService;
    [ObservableProperty]
    bool _isRefreshing;

    public TrackerViewModel(TrackerService trackerService)
    {
        _trackerService = trackerService;
    }

    [RelayCommand]
    public async Task GetTrackers()
    {
        try
        {
            //IsRefreshing = true;
            var trackers = await _trackerService.GetTrackers();
            if (Trackers.Count() != 0)
            {
                Trackers.Clear();
            }
            foreach(var tracker in trackers)
            {
                Trackers.Add(tracker);
            }
        }
        finally
        {
            IsRefreshing = false;
        }
    }

    [RelayCommand]
    async Task AddNewTracker()
    {
        await Shell.Current.GoToAsync($"{nameof(AddTrackerPage)}");
    }

    [RelayCommand]
    async Task GoToDetails(Tracker tracker)
    {
        if (tracker == null)
        {
            return;
        }

        var canon = _trackerService.GetCanon(tracker.CanonId);
        tracker.Canon = canon;
        await Shell.Current.GoToAsync(nameof(TrackerDetailsPage), true, new Dictionary<string, object>
        {
            {"Tracker", tracker }
        });
    }

    [RelayCommand]
    async Task GoToEdit(Tracker tracker)
    {
        if (tracker == null)
        {
            return;
        }

        await Shell.Current.GoToAsync(nameof(EditTrackerPage), true, new Dictionary<string, object>
        {
            {"Tracker", tracker }
        });
    }
}
