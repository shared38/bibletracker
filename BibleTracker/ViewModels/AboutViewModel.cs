﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using BibleTracker.Models;

namespace BibleTracker.ViewModels;

public partial class AboutViewModel : ObservableObject
{
    [ObservableProperty]
    About _about;

    public AboutViewModel()
    {
        _about = new About();
    }

    [RelayCommand]
    async Task LearnMore()
    {
        await Launcher.Default.OpenAsync(_about.MoreInfoUrl);
    }

    [RelayCommand]
    async void GoBack()
    {
        await Shell.Current.GoToAsync("..");
    }
}
