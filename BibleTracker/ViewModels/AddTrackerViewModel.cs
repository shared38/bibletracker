﻿using BibleTracker.Models;
using BibleTracker.Pages.Trackers;
using BibleTracker.Services;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using System.Collections.ObjectModel;

namespace BibleTracker.ViewModels;

public partial class AddTrackerViewModel : ObservableObject
{
    private TrackerService _trackerService;
    [ObservableProperty]
    private Tracker _newTracker;
    public ObservableCollection<Canon> Canons { get; } = new();
    [ObservableProperty]
    private Canon _selectedCanon;

    public AddTrackerViewModel(TrackerService trackerService)
    {
        _trackerService = trackerService;
        _newTracker = new Tracker()
        {
            Name = "Add New Test",
            CanonId = 1,
            IsActive = true,
            StartTime = DateTime.Now
        };
        var canons = _trackerService.GetCanons();
        foreach(var canon in canons)
        {
            Canons.Add(canon);
        }
    }

    [RelayCommand]
    public async void SaveNewTracker()
    {
        _newTracker.Canon = SelectedCanon;
        _trackerService.AddTracker(_newTracker);
        await Shell.Current.GoToAsync("..");
    }

    [RelayCommand]
    public async void CancelNewTracker()
    {
        await Shell.Current.GoToAsync("..");
    }
}
