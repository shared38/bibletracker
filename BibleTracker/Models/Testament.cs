﻿using SQLite;

namespace BibleTracker.Models;

public class Testament
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    [Unique]
    public string Name { get; set; }
}
