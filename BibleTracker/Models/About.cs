﻿namespace BibleTracker.Models
{
    public class About
    {
        public string Title => AppInfo.Name;
        public string Version => AppInfo.VersionString;
        public string MoreInfoUrl => "http://frankluke.wordpress.com/";
        public string Message => "Coded by Frank B. Luke";
    }
}
