﻿using SQLite;
using System.ComponentModel.DataAnnotations.Schema;

namespace BibleTracker.Models;

public class TrackerChapter
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    [ForeignKey("Tracker")]
    public int TrackerId { get; set; }
    [ForeignKey("Book")]
    public int BookId { get; set; }
    public int ChapterNumber { get; set; }
    public DateTime? DateRead { get; set; }

    [Ignore]
    public virtual Tracker Tracker { get; set; }
    [Ignore]
    public virtual Book Book { get; set; }
}
