﻿using SQLite;
using System.ComponentModel.DataAnnotations.Schema;

namespace BibleTracker.Models;

public class Tracker
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    [Unique]
    public string Name { get; set; }
    [ForeignKey("Canon")]
    public int CanonId { get; set; }
    public DateTime StartTime { get; set; }
    public DateTime? EndTime { get; set; }
    public bool IsActive { get; set; }

    [Ignore]
    public virtual Canon Canon { get; set; }
}
