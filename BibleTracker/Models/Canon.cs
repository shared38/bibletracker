﻿using SQLite;

namespace BibleTracker.Models;

public class Canon
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    [Unique]
    public string Name { get; set; }
    [Ignore] 
    public virtual List<Division> Divisions { get; set; }
}
