﻿using SQLite;
using System.ComponentModel.DataAnnotations.Schema;

namespace BibleTracker.Models;

public class Division
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public string Name { get; set; }
    [ForeignKey("Canon")]
    public int CanonId { get; set; }
    [ForeignKey("Testament")]
    public int TestamentId { get; set; }

    [Ignore]
    public virtual Canon Canon { get; set; }
    [Ignore]
    public virtual Testament Testament { get; set; }
    [Ignore]
    public virtual List<Book> Books { get; set; }
}
