﻿using SQLite;
using System.ComponentModel.DataAnnotations.Schema;

namespace BibleTracker.Models;

public class Book
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public string Name { get; set; }
    public int SortOrder { get; set; }
    public int NumberOfChapters { get; set; }
    [ForeignKey("Division")]
    public int DivisionId { get; set; }

    [Ignore]
    public virtual Division Division { get; set; }
}
