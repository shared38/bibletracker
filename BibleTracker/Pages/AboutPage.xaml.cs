using BibleTracker.ViewModels;

namespace BibleTracker.Pages;

public partial class AboutPage : ContentPage
{
	public AboutPage(AboutViewModel viewModel)
	{
		InitializeComponent();
        BindingContext = viewModel;
	}
}