using BibleTracker.ViewModels;

namespace BibleTracker.Pages.Trackers;

public partial class TrackerIndexPage : ContentPage
{
	public TrackerIndexPage(TrackerViewModel viewModel)
	{
		InitializeComponent();
		BindingContext = viewModel;
	}
}