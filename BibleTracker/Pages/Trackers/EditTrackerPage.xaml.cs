using BibleTracker.ViewModels;

namespace BibleTracker.Pages.Trackers;

public partial class EditTrackerPage : ContentPage
{
	public EditTrackerPage(EditTrackerViewModel viewModel)
	{
		InitializeComponent();
        BindingContext = viewModel;
    }
}