using BibleTracker.ViewModels;

namespace BibleTracker.Pages.Trackers;

public partial class TrackerDetailsPage : ContentPage
{
	public TrackerDetailsPage(TrackerDetailsViewModel viewModel)
	{
		InitializeComponent();
		BindingContext = viewModel;
	}
}