using BibleTracker.ViewModels;

namespace BibleTracker.Pages.Trackers;

public partial class AddTrackerPage : ContentPage
{
	public AddTrackerPage(AddTrackerViewModel viewModel)
	{
		InitializeComponent();
		BindingContext = viewModel;
	}
}