﻿using BibleTracker.Pages;
using BibleTracker.Pages.Trackers;

namespace BibleTracker;

public partial class AppShell : Shell
{
	public AppShell()
	{
		InitializeComponent();

		Routing.RegisterRoute(nameof(AboutPage), typeof(AboutPage));
		Routing.RegisterRoute(nameof(TrackerIndexPage), typeof(TrackerIndexPage));
        Routing.RegisterRoute(nameof(AddTrackerPage), typeof(AddTrackerPage));
		Routing.RegisterRoute(nameof(TrackerDetailsPage), typeof(TrackerDetailsPage));
		Routing.RegisterRoute(nameof(EditTrackerPage), typeof(EditTrackerPage));
    }
}
