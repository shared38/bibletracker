﻿using BibleTracker.Pages;
using BibleTracker.Pages.Trackers;

namespace BibleTracker;

public partial class MainPage : ContentPage
{
	int count = 0;

	public MainPage()
	{
		InitializeComponent();
	}

	private async void OnAboutClicked(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync($"{nameof(AboutPage)}");
	}

	private async void OnListTrackersClicked(object sender, EventArgs e)
	{
		await Shell.Current.GoToAsync($"{nameof(TrackerIndexPage)}");
	}

    private void OnCounterClicked(object sender, EventArgs e)
	{
		count++;

		if (count == 1)
			CounterBtn.Text = $"Clicked {count} time";
		else
			CounterBtn.Text = $"Clicked {count} times";

		SemanticScreenReader.Announce(CounterBtn.Text);
	}
}

